package wq

import "errors"

var (
	// ErrorDuplicate is returnd for duplicated tasks
	ErrorDuplicate = errors.New("Dropped task as duplicate")
)
