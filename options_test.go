package wq

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestOptions(t *testing.T) {
	var (
		maxWorkers               = 1
		maxTasks                 = 6
		workerTaskBuffer         = 3
		queueLoop                = 1 * time.Second
		retryAfter               = 1 * time.Hour
		verbose                  = true
		workerTerminationTimeout = 60 * time.Second
		queueLength              = 66
	)

	s := NewScheduler(
		WithMaxWorkers(maxWorkers),
		WithMaxTasks(maxTasks),
		WithWorkerTaskBuffer(workerTaskBuffer),
		WithQueueLoopSeconds(queueLoop.Seconds()),
		WithRetryAfterSeconds(retryAfter.Seconds()),
		WithVerbose(),
		WithWorkerTerminationTimeoutSeconds(workerTerminationTimeout.Seconds()),
		WithQueueLength(queueLength),
	)

	assert.Equal(t, maxWorkers, s.maxWorkers)
	assert.Equal(t, maxTasks, s.maxTasks)
	assert.Equal(t, workerTaskBuffer, s.workerTaskBuffer)
	assert.Equal(t, queueLoop, s.queueLoop)
	assert.Equal(t, retryAfter, s.retryAfter)
	assert.Equal(t, verbose, s.verbose)
	assert.Equal(t, workerTerminationTimeout, s.workerTerminationTimeout)
	assert.Equal(t, queueLength, s.queueLength)
	assert.Equal(t, DefaultMaxRetries, s.maxRetries)
}
