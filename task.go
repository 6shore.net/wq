package wq

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/oklog/ulid/v2"
)

type TaskInterface interface{}

type Task struct {
	// Unique task identification
	ID string
	// Queue key, default will be used when empty
	Key string

	// Object which will be passed to worker
	Obj TaskInterface

	// Timeout for processing this task
	//Timeout time.Duration

	// Timing options
	Queued   time.Time
	Started  time.Time
	Finished time.Time

	// Task result
	Result     error
	RetryAfter time.Time
	// How long wait before retry
	RetryTimeout time.Duration

	// number of failures for this task
	failures int

	// Function for processing this task
	// It will be executed by worker
	ProcessFunc ProcessFunc

	l sync.Mutex
}

func NewTask(key string, obj TaskInterface, pf ProcessFunc) *Task {
	t := Task{
		ID:          ulid.Make().String(),
		Key:         key,
		Obj:         obj,
		Queued:      time.Now(),
		ProcessFunc: pf,
	}

	return &t
}

// Validate if task is correct
func (t *Task) Validate() error {
	if t.Key == "" {
		return fmt.Errorf("Key is missing")
	}

	return nil
}

func (t *Task) Run(ctx context.Context) {
	t.l.Lock()
	defer t.l.Unlock()

	t.Result = t.ProcessFunc(ctx, t.Obj)
}

// Should wait indicate if this task is in Retry period
func (t *Task) WaitMore() time.Duration {
	return time.Until(t.RetryAfter)

}

// GetResult returns task result
func (t *Task) GetResult() error {
	t.l.Lock()
	defer t.l.Unlock()

	return t.Result
}

// GetRetryAfter returns retry after
func (t *Task) GetRetryAfter() time.Time {
	t.l.Lock()
	defer t.l.Unlock()

	return t.RetryAfter
}

// SetRetryAfter returns retry after
func (t *Task) setRetryAfter(i time.Time) {
	t.l.Lock()
	defer t.l.Unlock()

	t.RetryAfter = i
}

// GetFailures returns number of task failures
func (t *Task) GetFailures() int {
	t.l.Lock()
	defer t.l.Unlock()

	return t.failures
}

// SetFailures sets number of failures direclty
// It's useful when external tool is adding task to schedulers
// and this task was already failed before.
func (t *Task) SetFailures(n int) {
	t.l.Lock()
	defer t.l.Unlock()

	t.failures = n
}

// incFailures increases number of failures
func (t *Task) incFailures(n int) {
	t.l.Lock()
	defer t.l.Unlock()

	t.failures++
}
