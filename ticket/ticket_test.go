package ticket_test

import (
	"context"
	"log"
	"sync"
	"testing"
	"time"

	"go.6shore.net/wq/ticket"
)

func TestReservation(t *testing.T) {
	capacity := 3
	m := ticket.NewMachine(capacity)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	lofu := make(chan func(), capacity*2)

	// create locking functions
	for i := 0; i < capacity*2; i++ {
		jobID := i
		fu := func() {
			log.Printf("Function %d waiting reservation\n", jobID)

			waitCtx, cancel := context.WithTimeout(ctx, 100*time.Millisecond)
			defer cancel()

			err := m.Reserve(waitCtx)
			log.Printf("Job %d reservation result: %s", jobID, err)

			if jobID < capacity {
				// should get ticket
				if err != nil {
					t.Errorf("Job %d should get ticket", jobID)
				}
			} else {
				// should not get ticket
				if err == nil {
					t.Errorf("Job %d should NOT get ticket", jobID)
				}
			}

			log.Printf("Function %d finished\n", jobID)
		}

		log.Printf("Prepared fuction %d", jobID)
		lofu <- fu
	}

	// run locking functions
	wg := sync.WaitGroup{}
	for i := 0; i < capacity*2; i++ {
		fu := <-lofu

		wg.Add(1)
		go func() {
			defer wg.Done()
			fu()
		}()

		// wait some time so function have time do to reservation and
		// reservations are in order
		time.Sleep(10 * time.Millisecond)
	}
	wg.Wait()
}

func TestTermination(t *testing.T) {
	capacity := 5
	m := ticket.NewMachine(capacity)

	ctx := context.Background()
	var terminated bool

	for i := 0; i < capacity*5; i++ {
		if i >= capacity*2 {
			log.Printf("Terminating")

			// cal repeatedly
			m.Terminate()
			terminated = true
		}

		err := m.Reserve(ctx)
		if err == nil {
			m.Release()
		}
		log.Printf("Reserved %d with result: %s", i, err)

		if terminated && err == nil {
			t.Errorf("Job %d should not pass reservation", i)
		}
	}

}
