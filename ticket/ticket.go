package ticket

import (
	"context"
	"fmt"
	"sync"
)

type Machine interface {
	// Reserve is waiting for available ticket
	// abort on closed ctx or when machine is closed
	Reserve(context.Context) error

	// Report report action was finished and ticket can be returned back to be used again
	Release()

	// Terminate will stop accepting new tickets and wait for all tickets to be returned
	// Waiting ticket will be refused
	Terminate()
}

func NewMachine(capacity int) Machine {
	m := &manager{
		capacity:   capacity,
		ticketChan: make(chan *ticket, capacity),
		closing:    make(chan interface{}),
	}

	// create new tickets
	for i := 0; i < m.capacity; i++ {
		m.Release()
	}

	return m
}

type ticket struct {
	closing bool
}

type manager struct {
	capacity   int
	ticketChan chan *ticket
	closing    chan interface{}
}

func (o *manager) Reserve(ctx context.Context) error {
	select {
	// closed before starting waiting
	case <-o.closing:
		return fmt.Errorf("ticket manager is closing")
	default:
	}

	// try to read ticket from manager
	select {
	case tk := <-o.ticketChan:
		if tk == nil || tk.closing {
			// TODO: use typed error
			return fmt.Errorf("ticket manager is closed")
		}
	// closed during waiting for ticket
	case <-o.closing:
		return fmt.Errorf("ticket manager is closing")
	case <-ctx.Done():
		return fmt.Errorf("closed context while waiting for ticket reservation: %w", ctx.Err())
	}

	return nil
}

func (o *manager) Release() {
	// done will report finished action and create new ticket

	// write new ticket to manager
	select {
	case <-o.closing:
	case o.ticketChan <- &ticket{closing: false}:
	}

}
func (o *manager) Terminate() {
	// close closing channel if not closed already
	select {
	case <-o.closing:
		return
	default:
		close(o.closing)
	}

	wg := sync.WaitGroup{}

	// write closing ticket for all slots
	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < o.capacity; i++ {
			o.ticketChan <- &ticket{closing: true}
		}

	}()

	wg.Add(1)
	go func() {
		defer wg.Done()

	exit:
		for {
			for tk := range o.ticketChan {
				// stop when we start getting closing tickets
				if tk != nil && tk.closing {
					break exit
				}
			}
		}

	}()

	wg.Wait()
}
