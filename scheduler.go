package wq

import (
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/oklog/ulid/v2"
)

type Scheduler struct {
	// Maximum number of workers running
	maxWorkers int

	// Maximum number of tasks executed by a single worker
	// anything <= 0 means unlimited
	maxTasks int

	// how many tasks can wait for single worker
	workerTaskBuffer int

	// workerTerminationTimeout defines how long we accept waiting for worker termination
	workerTerminationTimeout time.Duration

	// how long we wait before task retry
	retryAfter time.Duration

	// queue process loop - process queue every this time
	queueLoop time.Duration

	// internal queue with tasks waiting for worker
	q chan *Task

	// queueLength defines how many tasks can be waiting
	queueLength int

	// wp is map for routing tasks to workers
	wp map[string]*Worker

	// mutex for manipulation with worker list
	wpl sync.Mutex

	// stopWorker is channel for terminating workers
	// it received worker key
	stopWorkerChan chan string

	// resultChan is receiving finished/failed tasks
	resChan chan *Task

	// global lock for atomic scheduler operations
	l sync.Mutex

	// internal stop channel, it will be closed in case of scheduler termination
	intStop chan interface{}

	// fin is use for termination verification
	fin chan interface{}

	// boot is used for startus sequence wait
	boot chan interface{}

	// qpChan is close termination notificator used to stop queue processing
	qpChan chan interface{}

	// pq is trigger for queue processing
	pq chan interface{}

	// verbosity defines how many informatin is printed
	verbose bool

	running bool

	// scheduler name
	name string

	// how many times can be task added back to queue
	// -1 means unlimited
	maxRetries int
}

const (
	// DefaultRetryAfter sets default value for retry
	DefaultRetryAfter = 30 * time.Second

	// DefaultQueueLength
	DefaultQueueLength = 1000

	// DefaultMaxRetries set default max retries for scheduler
	DefaultMaxRetries = -1
)

func NewScheduler(opts ...SchedulerOpt) *Scheduler {
	// create scheduler with default values
	s := &Scheduler{
		// TODO: move these options to variables
		maxWorkers:               50,
		maxTasks:                 20,
		workerTaskBuffer:         10,
		queueLoop:                20 * time.Second,
		workerTerminationTimeout: 300 * time.Second,

		retryAfter:  DefaultRetryAfter,
		queueLength: DefaultQueueLength,

		maxRetries: DefaultMaxRetries,
	}

	// apply scheduler options
	for _, of := range opts {
		of(s)
	}

	// set random name
	if s.name == "" {
		s.name = ulid.Make().String()
	}

	return s
}

// Name returns scheduler name
func (s *Scheduler) Name() string {
	return s.name
}

// lpf logs when verbose is enabled
func (s *Scheduler) lpf(format string, v ...interface{}) {
	if !s.verbose {
		return
	}

	f := fmt.Sprintf("wq %s: %s", s.Name(), format)
	log.Printf(f, v...)
}

// lp log every time
func (s *Scheduler) lp(format string, v ...interface{}) {
	f := fmt.Sprintf("wq %s: %s", s.Name(), format)
	log.Printf(f, v...)
}

func (s *Scheduler) Start() error {
	s.lpf("Scheduler start requested")
	s.l.Lock()
	defer s.l.Unlock()

	if !s.running {
		s.init()

		// start handler for results
		go s.resultProcessor(s.fin)

		// start handler for processing queue
		go s.processQueueProcessor(s.fin)

		// start handler for terminated workers
		go s.stopWorkerProcessor(s.fin)

		s.lpf("Waiting for component boot")
		<-s.boot
		s.lpf("One up")
		<-s.boot
		s.lpf("Two up")
		<-s.boot
		s.lpf("Components booted")

		s.running = true
	} else {
		s.lp("Scheduler can't be started because its running")
	}

	return nil
}

func (s *Scheduler) init() {
	s.lpf("Executing init")

	// set channels
	s.q = make(chan *Task, s.queueLength)
	s.wp = map[string]*Worker{}
	s.pq = make(chan interface{})

	s.intStop = make(chan interface{})
	s.fin = make(chan interface{})
	s.boot = make(chan interface{})
	s.qpChan = make(chan interface{})
}

// Stop terminates all running workers and stop
func (s *Scheduler) Stop() error {
	s.lpf("Stop scheduler requested")
	s.l.Lock()
	defer s.l.Unlock()

	if s.running {
		// stop queue processing and wait for fin ack
		close(s.qpChan)
		<-s.fin

		// stop workers by channel
		s.wpl.Lock()
		s.lpf("Going to stop all workers")
		wg := sync.WaitGroup{}
		for _, w := range s.wp {
			wg.Add(1)
			go func(key string) {
				s.stopWorkerChan <- key
				wg.Done()
			}(w.key)
		}
		s.wpl.Unlock()
		wg.Wait()

		// wait for termination of all workers
		// TODO: remove loop
		maxTime := time.Now().Add(s.workerTerminationTimeout)
		for {
			ww := time.Until(maxTime)
			if ww <= 0 {
				return fmt.Errorf("Timeout while waiting for worker temination: %v", s.wp)
			}

			s.wpl.Lock()
			if w := s.wp; len(w) == 0 {
				s.lpf("All workers are terminated")
				s.wpl.Unlock()
				break
			} else {
				s.lpf("Some workers still running (will wait %s): %v", ww, w)
				time.Sleep(300 * time.Millisecond)
			}
			s.wpl.Unlock()

		}

		// terminate itself
		close(s.intStop)
		<-s.fin
		<-s.fin

		close(s.fin)

		s.running = false
	} else {
		s.lp("Scheduler can't be stopped because its not running")
	}

	return nil
}

func (s *Scheduler) WorkerCount() int {
	s.wpl.Lock()
	defer s.wpl.Unlock()

	return len(s.wp)
}

func (s *Scheduler) GetWorker(key string) (*Worker, error) {
	s.wpl.Lock()
	defer s.wpl.Unlock()

	w, ok := s.wp[key]
	if !ok || w == nil {
		return nil, fmt.Errorf("Missing worker for key %s", key)
	}

	return w, nil
}

// Tries to start worker
func (s *Scheduler) startWorker(key string) error {
	// terminate if worker already exist
	if _, err := s.GetWorker(key); err == nil {
		return nil
	}

	// can we start new worker?
	if s.WorkerCount() >= s.maxWorkers {
		return fmt.Errorf("Maximum number of workers (%d) reached", s.maxWorkers)
	}

	rc := make(chan *Task, s.workerTaskBuffer)

	// start new worker
	worker := NewWorker(key, rc, s.resChan, s.stopWorkerChan)

	// pass configuration from scheduler
	worker.maxTasks = s.maxTasks
	worker.verbose = s.verbose

	s.wpl.Lock()
	s.wp[key] = worker
	s.wpl.Unlock()

	// prepare task in worker queue
	go func() {
		s.ProcessQueue()
	}()

	worker.Start()
	s.lp("Started worker for key %s", key)

	return nil
}

// Will try to stop worker
func (s *Scheduler) stopWorker(key string) error {
	s.lpf("Stop worker with with %s", key)
	w, err := s.GetWorker(key)
	if err != nil {
		s.lpf("Worker %s doesn't exist: %s", key, err)
		return nil
	}
	w.Stop()
	s.lpf("Worker %s Stop() finished", key)

	// remove from router
	s.wpl.Lock()
	defer s.wpl.Unlock()
	delete(s.wp, key)
	s.lpf("Worker %s removed from router", key)

	// put remaining tasks back to queue
	s.flushWorkerQueue(w.rc)

	// close worker channel
	close(w.rc)

	s.lp("Worker %s termination (via scheduler) finished", key)

	return nil
}

func (s *Scheduler) flushWorkerQueue(wch <-chan *Task) {
	// read all reamining task
	waitingTasks := []*Task{}
bf:
	for {
		select {
		case rem := <-wch:
			// add task back to queue
			waitingTasks = append(waitingTasks, rem)
		default:
			break bf
		}
	}

	// write remaining tasks back to queue
	for _, t := range waitingTasks {
		s.q <- t
	}
}

// ProcessQueue trigger queue processing
func (s *Scheduler) ProcessQueue() {
	/*
		if !s.IsRunning() {
			return
		}
	*/

	s.pq <- true
}

func (s *Scheduler) QueueTask(t *Task) error {
	/*
		if !s.IsRunning() {
			return fmt.Errorf("Scheduler is terminated")
		}
	*/

	if err := t.Validate(); err != nil {
		return fmt.Errorf("Failed adding task to queue, task is invalid: %s", err)
	}

	t.Queued = time.Now()

	// add to task to queue
	s.q <- t
	s.lp("Task %s added to queue with key %s", t.ID, t.Key)

	// trigger queue processing
	go func() {
		s.ProcessQueue()
	}()

	return nil
}
