package wq

import (
	"context"
	"fmt"
	"log"
	"time"
)

type ProcessFunc func(context.Context, TaskInterface) error

type Worker struct {
	// channel for receiving tasks
	rc chan *Task

	// channel for sending termination notification
	stopChan chan<- string

	// resultChan is used for sending results
	resChan chan<- *Task

	// internal termination channel
	intStop chan interface{}

	// fin is used to verify everything stopped
	fin chan interface{}

	// Queue key
	key string

	// Number of tasks processed by this worker
	taskCount int

	// Max tasks executed (successful + failures) executed by this worker
	// Worker will terminate after finishing last task
	maxTasks int

	// Time when this worker started
	started time.Time

	// verbose enables more verbose logging
	verbose bool

	// worker context passed to all tasks
	ctx context.Context

	// cancel function used to terminate running tasks
	cancel context.CancelFunc
}

func NewWorker(key string, rc chan *Task, resChan chan<- *Task, stopChan chan<- string) *Worker {
	return &Worker{
		rc:       rc,
		stopChan: stopChan,
		resChan:  resChan,
		intStop:  make(chan interface{}),
		fin:      make(chan interface{}),

		key:       key,
		taskCount: 0,
	}
}

// lpf logs when verbose is enabled
func (w *Worker) lpf(format string, v ...interface{}) {
	f := fmt.Sprintf("wq: Worker %s: %s", w.key, format)
	if w.verbose {
		log.Printf(f, v...)
	}
}

func (w *Worker) lp(format string, v ...interface{}) {
	f := fmt.Sprintf("wq: Worker %s: %s", w.key, format)
	log.Printf(f, v...)
}

func (w *Worker) GetKey() string {
	return w.key
}

func (w *Worker) LifeTime() time.Duration {
	return time.Since(w.started)
}

// Start worker
func (w *Worker) Start() {
	w.lp("starting worker")
	w.started = time.Now()

	w.ctx, w.cancel = context.WithCancel(context.Background())

	go w.work()
}

// Stop wait for current task and stop worker
func (w *Worker) Stop() {
	w.lpf("stopping worker")

	// close worker context
	w.cancel()

	// send internal notification to stop
	select {
	case _, ok := <-w.intStop:
		if !ok {
			w.lp("intStop already closed, won't close it")
		}
	default:
		w.lp("closing intStop")
		close(w.intStop)
	}
	w.lpf("stop signal send (intStop closed), waiting for termination")

	// wait for termination by closing fin channel
	// it can be closed already
begin:
	select {
	case _, ok := <-w.fin:
		if ok {
			goto begin
		}
		break
	default:
		time.Sleep(100 * time.Millisecond)
		goto begin
	}

	w.lp("terminated")
}

// work is reading tasks from channels and executing it
func (w *Worker) work() {
	// stop worker when all tasks are done or termination signal is received
	defer func() {
		close(w.fin)
	}()

	for {
		select {
		// terminate on request - closed channel
		case _, ok := <-w.intStop:
			if !ok {
				w.lpf("terminating work loop")
				return
			}

		// do work
		case t := <-w.rc:
			w.taskCount++
			w.lp("Doing task %s (%d/%d)", t.ID, w.taskCount, w.maxTasks)
			w.doTask(t)

			// terminate if maxTasks was reached
			if w.maxTasks > 0 && w.taskCount >= w.maxTasks {
				w.lpf("Worker %s reached maxTasks %d by doing %d terminating", w.key, w.maxTasks, w.taskCount)
				w.stopChan <- w.key
				return
			}

		// request termination on no more work
		default:
			if w.LifeTime() >= 2*time.Second {
				w.lpf("don't have any work to do")
				w.stopChan <- w.key
				return
			}

			time.Sleep(200 * time.Millisecond)
		}
	}
}

func (w *Worker) doTask(t *Task) {
	t.Started = time.Now()
	w.taskCount++

	w.lpf("executing task function %s", t.ID)

	// run task
	// TODO: add task timeout
	t.Run(w.ctx)

	w.lpf("task %s function finished", t.ID)

	// send result back to scheduler
	w.resChan <- t

	w.lpf("Finished sending task %s result", t.ID)
}
