package wq

// StringInSlice vefify existence of key in slice
// Copied from serelib, use directly when serelib is available
func stringInSlice(s string, list []string) bool {
	for _, v := range list {
		if v == s {
			return true
		}
	}
	return false
}
