package wq

import "time"

type SchedulerOpt func(*Scheduler)

// WithName sets scheduler name
func WithName(name string) SchedulerOpt {
	return func(s *Scheduler) {
		s.name = name
	}
}

// WithMaxRetries sets number or retries for tasks
// use WithMaxRetries(0) to disable any retry (tak will run only once)
// WithMaxRetries(1) means task will be retried once
// Default value is -1 meaning retry forever
func WithMaxRetries(n int) SchedulerOpt {
	return func(s *Scheduler) {
		s.maxRetries = n
	}
}

// WithMaxWorkers set maxWorkers for scheduler
func WithMaxWorkers(n int) SchedulerOpt {
	return func(s *Scheduler) {
		s.maxWorkers = n
	}
}

// WithMaxTasks set maxTasks for scheduler
func WithMaxTasks(n int) SchedulerOpt {
	return func(s *Scheduler) {
		s.maxTasks = n
	}
}

// WithWorkerTaskBuffer set buffer lenght for task
func WithWorkerTaskBuffer(n int) SchedulerOpt {
	return func(s *Scheduler) {
		if n > 0 {
			s.workerTaskBuffer = n
		}
	}
}

// WithQueueLoopSeconds sets how often scheduler read waiting queue
func WithQueueLoopSeconds(n float64) SchedulerOpt {
	return func(s *Scheduler) {
		if n > 0 {
			s.queueLoop = time.Duration(n) * time.Second
		}
	}
}

// WithRetryAfterSeconds set how long scheduler waits before retrying failed task
func WithRetryAfterSeconds(n float64) SchedulerOpt {
	return func(s *Scheduler) {
		if n > 0 {
			s.retryAfter = time.Duration(n) * time.Second
		}
	}
}

// WithVerbose enables verbose mode
func WithVerbose() SchedulerOpt {
	return func(s *Scheduler) {
		s.verbose = true
	}
}

// WithWorkerTerminationTimeoutSeconds defines how long we wait for worker
func WithWorkerTerminationTimeoutSeconds(n float64) SchedulerOpt {
	return func(s *Scheduler) {
		if n > 0 {
			s.workerTerminationTimeout = time.Duration(n) * time.Second
		}
	}
}

// WithQueueLength defines queue lenght for tasks
// Required to be > 10
func WithQueueLength(n int) SchedulerOpt {
	return func(s *Scheduler) {
		if n > 10 {
			s.queueLength = n
		}
	}
}
