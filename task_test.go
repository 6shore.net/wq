package wq_test

import (
	"context"
	"log"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.6shore.net/wq"
)

var _ = Describe("Task", func() {
	var (
		task *wq.Task
		key  string
		pf   wq.ProcessFunc
	)

	BeforeEach(func() {
		pf = func(_ context.Context, obj wq.TaskInterface) error {
			log.Printf("Processing task")

			return nil
		}
		key = "abc"
		task = wq.NewTask(key, nil, pf)
	})
	It("Have default fields", func() {
		Expect(task.ID).NotTo(BeEmpty())
		Expect(task.Key).To(Equal(key))
	})
})
