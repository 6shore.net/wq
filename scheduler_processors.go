package wq

import (
	"fmt"
	"log"
	"time"
)

func (s *Scheduler) stopWorkerProcessor(fin chan<- interface{}) {
	s.lpf("Starting worker exitus processor")
	defer func() {
		s.lpf("Terminated worker exitus processor")
		fin <- true
	}()

	s.stopWorkerChan = make(chan string, s.maxWorkers*3)
	s.boot <- true

	for {
		select {
		case key := <-s.stopWorkerChan:
			s.lpf("Scheduler is trying to stop worker %s", key)
			if err := s.stopWorker(key); err != nil {
				s.lpf("Failed to stop worker: %s", err)
			}
		case _, ok := <-s.intStop:
			if !ok {
				close(s.stopWorkerChan)
				return
			}
		}
	}
}

func (s *Scheduler) processQueueProcessor(fin chan<- interface{}) {
	s.lpf("Starting queue processor")
	defer func() {
		s.lpf("Terminated queue processor")
		fin <- true
	}()

	s.boot <- true

	for {
		select {
		case <-s.pq:
			s.doQueueLoop()
		case <-time.After(s.queueLoop):
			s.doQueueLoop()
		case _, ok := <-s.qpChan:
			if !ok {
				return
			}
		}
	}
}

func (s *Scheduler) doQueueLoop() {
	s.lpf("Processing queue")

	// counters
	countInQueue := 0
	countFailedSend := 0
	countSendToWorker := 0
	countWaiting := 0
	countRemovedDuplicates := 0
	countMissingWorkers := 0
	countBackQueue := 0

	// task manipulation
	tasks := map[string]*Task{}
	// try to send these tasks to workers
	trySend := []string{}
	// failed send to worker - will be returned back to queue
	failedSend := []string{}
	// will try to start these tasks
	missingWorkers := []string{}

	// remove duplicates
plf:
	for {
		select {
		case t := <-s.q:
			countInQueue++

			if prev, ok := tasks[t.ID]; ok {
				countRemovedDuplicates++
				if prev.GetFailures() > t.GetFailures() {
					// don't send to try send  anywhere
					s.lpf("Dropping task %s as duplicate", t.ID)
					t.Result = ErrorDuplicate
					continue
				} else {
					s.lpf("Overwriting previous task %s as duplicate", t.ID)
					tasks[t.ID].Result = ErrorDuplicate
				}
			}

			tasks[t.ID] = t

			// it can be in slice in case of task override
			if !stringInSlice(t.ID, trySend) {
				trySend = append(trySend, t.ID)
			}
		default:
			break plf
		}

	}

	// take all tasks from queue and try to send it
	for _, tid := range trySend {
		t := tasks[tid]
		if tw := t.WaitMore(); tw > 0 {
			countWaiting++
			s.lpf("Task %s is waiting, will wait %s, latest result: %s", t.ID, tw.Round(time.Second), t.Result)
			failedSend = append(failedSend, t.ID)
			continue
		}

		s.lpf("Routing task %s", t.ID)
		if err := s.sendTask(t); err != nil {
			countFailedSend++
			s.lpf("Failed to route task %s: %s", t.ID, err)
			failedSend = append(failedSend, t.ID)
		} else {
			countSendToWorker++
			s.lpf("Task %s send to worker queue %s", t.ID, t.Key)
		}
	}

	// find missing workers
	for _, tid := range failedSend {
		t := tasks[tid]
		if t.WaitMore() <= 0 && !stringInSlice(t.Key, missingWorkers) {
			missingWorkers = append(missingWorkers, t.Key)
		}
	}

	// try to start missing workers
	countMissingWorkers = len(missingWorkers)
	for _, mk := range missingWorkers {
		if err := s.startWorker(mk); err != nil {
			s.lpf("Failed starting worker %s: %s", mk, err)
		}
	}

	// return tasks back to queue
	for _, tid := range failedSend {
		countBackQueue++
		s.q <- tasks[tid]
	}

	// do report
	s.lp("Workers %d/%d, InQueue %d, FailedSend %d, SendToWorker %d, WaitingTasks %d, RemovedDuplicates %d, MissingWorkers %d, ReturnedToQueue %d",
		s.WorkerCount(), s.maxWorkers, countInQueue, countFailedSend, countSendToWorker, countWaiting, countRemovedDuplicates, countMissingWorkers, countBackQueue)
}

func (s *Scheduler) sendTask(t *Task) error {
	s.lpf("Sending task %s", t.ID)

	s.wpl.Lock()
	defer s.wpl.Unlock()
	w, ok := s.wp[t.Key]
	if !ok {
		return fmt.Errorf("Worker for key %s is not available", t.Key)
	}

	select {
	case w.rc <- t:
		return nil
	default:
		return fmt.Errorf("Failed to send task %s to worker, can't write to channel", t.ID)
	}
}

func (s *Scheduler) resultProcessor(fin chan<- interface{}) {
	s.lpf("Starting result processor")
	defer func() {
		s.lpf("Terminated result processor")
		fin <- true
	}()

	s.resChan = make(chan *Task)
	s.boot <- true

	for {
		select {
		case t := <-s.resChan:
			if res := t.GetResult(); res == nil {
				s.lp("Task %s is finished", t.ID)
				break
			}

			// increase error count
			t.incFailures(1)

			if s.maxRetries >= 0 && t.GetFailures() > s.maxRetries {
				s.lp("Task %s failed %d/%d times, discarding the task", t.ID, t.GetFailures(), s.maxRetries)
				break
			}

			var w time.Duration
			if t.RetryTimeout > 0 {
				w = t.RetryTimeout * time.Duration(t.GetFailures())
			} else {
				w = s.retryAfter * time.Duration(t.GetFailures())
			}
			t.setRetryAfter(time.Now().Add(w))

			wt := time.Until(t.RetryAfter).Round(time.Second)

			s.lp("Task %s failed with error: '%s', moving back to queue, have %d/%d failures, retry after %s", t.ID, t.Result, t.GetFailures(), s.maxRetries, wt)
			if err := s.QueueTask(t); err != nil {
				log.Printf("Unable to return task to queue: %s", err)
			}

		case _, ok := <-s.intStop:
			if !ok {
				close(s.resChan)
				return
			}
		}
	}
}
