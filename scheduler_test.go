package wq_test

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/oklog/ulid/v2"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.6shore.net/wq"
)

var _ = Describe("Scheduler", func() {
	type TestTask struct {
		UID  string
		Note string
	}

	var (
		// normal onw
		pf wq.ProcessFunc

		// always failing
		pff wq.ProcessFunc

		useFunction wq.ProcessFunc

		key           string
		scheduler     *wq.Scheduler
		task          *wq.Task
		dir           string
		schedulerOpts []wq.SchedulerOpt

		ct int
	)

	BeforeEach(func() {
		pf = func(_ context.Context, obj wq.TaskInterface) error {
			tt := obj.(*TestTask)
			log.Printf("Processing inside task %s", tt.UID)

			fi := filepath.Join(dir, tt.UID)
			if err := os.WriteFile(fi, []byte("hello"), 0644); err != nil {
				return err
			}
			log.Printf("Writing test file to %s", fi)
			log.Printf("----- TASK DONE -----")

			return nil
		}

		pff = func(_ context.Context, obj wq.TaskInterface) error {
			tt := obj.(*TestTask)
			log.Printf("Processing inside task %s", tt.UID)

			return fmt.Errorf("This task will fail always, object %+v", tt)
		}

		// use working function by default
		useFunction = pf

		// generate test data
		key = "abc"

		ct = 0

	})

	JustBeforeEach(func() {
		var err error

		scheduler = wq.NewScheduler(schedulerOpts...)
		i := &TestTask{
			UID:  ulid.Make().String(),
			Note: "original",
		}
		task = wq.NewTask(key, i, useFunction)
		task.ID = i.UID

		// prepare testing directory
		dir, err = os.MkdirTemp("", "SchedulerTest")
		Expect(err).To(Succeed())
	})

	JustBeforeEach(func() {
		Expect(scheduler.Start()).To(Succeed())
	})

	AfterEach(func() {
		Expect(scheduler.Stop()).To(Succeed())
		os.RemoveAll(dir)
	})

	Describe("Scheduler operations", func() {
		It("Shouldn't fail on double stop", func() {
			Expect(scheduler.Stop()).To(Succeed())
			Expect(scheduler.Stop()).To(Succeed())
		})

		It("Shouldn't fail on double start", func() {
			Expect(scheduler.Start()).To(Succeed())
			Expect(scheduler.Start()).To(Succeed())
			log.Printf("Double stop done, executing start")
			Expect(scheduler.Stop()).To(Succeed())
		})

		It("Have empty", func() {
			Expect(scheduler.WorkerCount()).To(Equal(0))
		})
	})

	Describe("Default settings", func() {
		BeforeEach(func() {
			useFunction = pff
			schedulerOpts = []wq.SchedulerOpt{
				wq.WithVerbose(),
			}

		})
		JustBeforeEach(func() {
			task.SetFailures(200)
		})
		It("Have name", func() {
			Expect(scheduler.Name()).ToNot(BeEmpty())
		})
		It("Sets RetryAfter from scheduler", func() {
			ct := time.Now()
			Expect(scheduler.QueueTask(task)).To(Succeed())

			// task should fail
			Eventually(func() error {
				return task.GetResult()
			}, "5s", "50ms").Should(HaveOccurred())

			w := wq.DefaultRetryAfter * time.Duration(task.GetFailures())
			// RetryAfter should be set
			Eventually(func() time.Time {
				return task.GetRetryAfter()
			}, "1s", "100ms").Should(BeTemporally(">=", ct, w))

		})
		It("Sets RetryAfter from task", func() {
			ct := time.Now()
			task.RetryTimeout = 100 * time.Hour
			Expect(scheduler.QueueTask(task)).To(Succeed())

			// task should fail
			Eventually(func() error {
				return task.GetResult()
			}, "5s", "50ms").Should(HaveOccurred())

			w := task.RetryTimeout * time.Duration(task.GetFailures())

			// RetryAfter should be set
			Expect(task.GetResult()).To(HaveOccurred())

			// wait for worker termination
			Eventually(func() int {
				return scheduler.WorkerCount()
			}, "5s", "50ms").Should(Equal(0))

			Expect(task.GetRetryAfter()).To(BeTemporally(">=", ct, w))
		})

	})

	Describe("Syntetic tasks", func() {
		BeforeEach(func() {
			// set very fast retry
			// very frequent worker restart
			schedulerOpts = []wq.SchedulerOpt{
				wq.WithMaxTasks(4),
				wq.WithQueueLoopSeconds(0.1),
				wq.WithRetryAfterSeconds(0.1),
				wq.WithVerbose(),
				wq.WithWorkerTerminationTimeoutSeconds(15),
			}
		})
		It("Executes task", func() {
			err := scheduler.QueueTask(task)
			Expect(err).To(Succeed())

			Eventually(func() string {
				fi := filepath.Join(dir, task.ID)
				return fi
			}, "1s", "50ms").Should(BeAnExistingFile())
		})

		It("Runs many tasks", func() {
			taskCount := 15

			tf := func(_ context.Context, obj wq.TaskInterface) error {
				tt := obj.(*TestTask)

				if tt.UID == fmt.Sprintf("%d", taskCount) {
					fi := filepath.Join(dir, tt.UID)
					if err := os.WriteFile(fi, []byte("hello"), 0644); err != nil {
						return err
					}
					log.Printf("Writing test file to %s", fi)
				}

				log.Printf("----- TASK %s DONE -----", tt.UID)
				return nil
			}

			for i := 0; i <= taskCount; i++ {
				k := fmt.Sprintf("key-%d", int(i/3))
				t := wq.NewTask(k, &TestTask{UID: fmt.Sprintf("%d", i)}, tf)
				err := scheduler.QueueTask(t)
				Expect(err).To(Succeed())
			}

			// wait for last task
			Eventually(func() string {
				scheduler.ProcessQueue()

				fi := filepath.Join(dir, fmt.Sprintf("%d", taskCount))
				log.Printf("Checking for file: %s", fi)
				return fi
			}, "10s", "50ms").Should(BeAnExistingFile())
		})

		// Creates duplicate tasks with more failures and verifies
		// Task with Note="duplicate" should remain
		// Both failures
		It("Drops duplicates", func() {
			uid := ulid.Make().String()
			ra := 1 * time.Millisecond

			i := &TestTask{
				UID:  uid,
				Note: "first",
			}
			taskFirst := wq.NewTask(key, i, pff)
			taskFirst.ID = uid
			taskFirst.RetryTimeout = ra

			i = &TestTask{
				UID:  uid,
				Note: "second",
			}
			taskDup := wq.NewTask(key, i, pff)
			taskDup.ID = uid
			taskDup.SetFailures(task.GetFailures() + 10)
			taskDup.RetryTimeout = ra

			// schedule both tasks
			prevFailures := taskFirst.GetFailures() + taskDup.GetFailures()
			Expect(scheduler.QueueTask(taskFirst)).To(Succeed())
			Expect(scheduler.QueueTask(taskDup)).To(Succeed())

			// wait for 5 runs
			Eventually(func() int {
				return taskFirst.GetFailures() + taskDup.GetFailures()
			}, "10s", "50ms").Should(BeNumerically(">=", prevFailures+10))

			log.Printf("Have failures: %d", taskFirst.GetFailures()+taskDup.GetFailures())

			// taskDup should continue, task dropped
			Expect(taskFirst.GetResult()).To(MatchError(wq.ErrorDuplicate))

		})

		It("Restart worker when needed", func() {
			taskCount := 15

			tf := func(_ context.Context, obj wq.TaskInterface) error {
				tt := obj.(*TestTask)

				if tt.UID == fmt.Sprintf("%d", taskCount) {
					fi := filepath.Join(dir, tt.UID)
					if err := os.WriteFile(fi, []byte("hello"), 0644); err != nil {
						return err
					}
					log.Printf("Writing test file to %s", fi)
				}

				log.Printf("----- TASK %s DONE -----", tt.UID)
				return nil
			}

			for i := 0; i <= taskCount; i++ {
				k := "key-0"
				t := wq.NewTask(k, &TestTask{UID: fmt.Sprintf("%d", i)}, tf)
				err := scheduler.QueueTask(t)
				Expect(err).To(Succeed())
			}

			// wait for last task
			Eventually(func() string {
				scheduler.ProcessQueue()

				fi := filepath.Join(dir, fmt.Sprintf("%d", taskCount))
				log.Printf("Checking for file: %s", fi)
				return fi
			}, "10s", "50ms").Should(BeAnExistingFile())

		})

		It("Retries failing tasks", func() {
			tf := func(_ context.Context, obj wq.TaskInterface) error {
				tt := obj.(*TestTask)

				ct++

				if ct >= 3 {
					fi := filepath.Join(dir, tt.UID)
					if err := os.WriteFile(fi, []byte("hello"), 0644); err != nil {
						return err
					}
					log.Printf("Writing test file to %s", fi)

					log.Printf("----- TASK %s DONE, try %d -----", tt.UID, ct)
					return nil
				}

				log.Printf("Task %s fail, try %d", tt.UID, ct)
				return fmt.Errorf("Failed on %d try", ct)
			}

			tt := &TestTask{UID: ulid.Make().String()}
			t := wq.NewTask(key, tt, tf)
			t.RetryTimeout = 100 * time.Millisecond
			err := scheduler.QueueTask(t)
			Expect(err).To(Succeed())

			// wait for task
			Eventually(func() string {
				scheduler.ProcessQueue()
				fi := filepath.Join(dir, tt.UID)
				log.Printf("Checking for file: %s", fi)
				return fi
			}, "10s", "50ms").Should(BeAnExistingFile())
		})

	})

	Describe("Syntetic tasks with retry", func() {
		BeforeEach(func() {
			// set very fast retry
			// very frequent worker restart
			schedulerOpts = []wq.SchedulerOpt{
				wq.WithQueueLoopSeconds(0.3),
				wq.WithRetryAfterSeconds(0.0),
				wq.WithVerbose(),
				wq.WithWorkerTerminationTimeoutSeconds(5),
				wq.WithMaxRetries(5),
			}
		})

		It("Respects maxRetries", func() {
			// task will write file on first run and fails on every try
			tf := func(_ context.Context, obj wq.TaskInterface) error {
				tt := obj.(*TestTask)
				if ct == 5 {
					fi := filepath.Join(dir, tt.UID)
					if err := os.WriteFile(fi, []byte("hello"), 0644); err != nil {
						return err
					}
					log.Printf("Writing test file to %s", fi)
				} else {
					time.Sleep(100 * time.Millisecond)
				}

				ct++

				log.Printf("Task %s fail, try %d", tt.UID, ct)
				return fmt.Errorf("Failed on %d try", ct)
			}

			tt := &TestTask{UID: ulid.Make().String()}
			t := wq.NewTask(key, tt, tf)
			t.RetryTimeout = 1 * time.Millisecond
			err := scheduler.QueueTask(t)
			Expect(err).To(Succeed())

			// wait for task
			Eventually(func() string {
				fi := filepath.Join(dir, tt.UID)
				log.Printf("Checking for file: %s", fi)
				return fi
			}, "10s", "50ms").Should(BeAnExistingFile())

			Eventually(func() int {
				return t.GetFailures()
			}, "10s", "50ms").Should(Equal(6), "Task should not be retried")

			// wait short time and verify we have still 6 failures
			Consistently(func() int {
				return t.GetFailures()
			}, "2s", "50ms").Should(Equal(6), "Task retries should keep same")

		})
	})

	Describe("Syntetic tasks without retry", func() {
		BeforeEach(func() {
			// set very fast retry
			// very frequent worker restart
			schedulerOpts = []wq.SchedulerOpt{
				wq.WithQueueLoopSeconds(0.3),
				wq.WithRetryAfterSeconds(0.0),
				wq.WithVerbose(),
				wq.WithWorkerTerminationTimeoutSeconds(5),
				wq.WithMaxRetries(0),
			}
		})

		It("Runs single retry", func() {
			// task will write file on first run and fails on every try
			tf := func(_ context.Context, obj wq.TaskInterface) error {
				tt := obj.(*TestTask)
				fi := filepath.Join(dir, tt.UID)

				if ct == 0 {
					// create file on first run
					if err := os.WriteFile(fi, []byte("hello"), 0644); err != nil {
						return err
					}
					log.Printf("Writing test file to %s", fi)
				} else {
					// delete file on subsequent runs
					if err := os.Remove(fi); err != nil {
						return err
					}
				}

				ct++

				log.Printf("Task %s fail, try %d", tt.UID, ct)
				return fmt.Errorf("Failed on %d try", ct)
			}

			tt := &TestTask{UID: ulid.Make().String()}
			t := wq.NewTask(key, tt, tf)
			t.RetryTimeout = 1 * time.Millisecond
			err := scheduler.QueueTask(t)
			Expect(err).To(Succeed())

			// wait for task
			Eventually(func() string {
				return filepath.Join(dir, tt.UID)
			}, "5s", "50ms").Should(BeAnExistingFile())

			Eventually(func() int {
				return t.GetFailures()
			}, "5s", "50ms").Should(Equal(1), "Task should not be retried")

			// wait short time and verify we have still 1 failures
			Consistently(func() int {
				return t.GetFailures()
			}, "1s", "50ms").Should(Equal(1), "Task retries should keep same")

			Eventually(func() string {
				return filepath.Join(dir, tt.UID)
			}, "2s", "50ms").Should(BeAnExistingFile(), "Test file should still exist")

		})
	})
})
